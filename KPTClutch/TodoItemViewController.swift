//
//  TodoItemViewController.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 1/2/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

import UIKit
import CoreData

class TodoItemViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerView: UIPickerView!
    //    @IBOutlet weak var todoField: UITextView!
    var task: Todo? = nil
    var globalSelectedItem:String!
    var globalRowNumber:Int!
    var texts:NSArray = ["Onion", "Egg", "Becon", "Garlic"]
    private var preSelectedLb:UILabel!
    @IBAction func save(sender: UIBarButtonItem) {
        if task != nil {
            editTask()
        } else {
            createTask()
        }
        navigationController!.popViewControllerAnimated(true)
    }
    func createTask() {
        let newTask: Todo = Todo.MR_createEntity() as! Todo
        newTask.item = globalSelectedItem!
        newTask.managedObjectContext!.MR_saveToPersistentStoreAndWait()
    }
    
    func editTask() {
        task?.item = globalSelectedItem!
        task?.managedObjectContext!.MR_saveToPersistentStoreAndWait()
    }
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        navigationController!.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        pickerView.delegate = self
        pickerView.dataSource = self
        if let taskTodo = task {
            globalSelectedItem = taskTodo.item
        }
        if globalSelectedItem == nil {
            globalSelectedItem = texts[0] as! String
        }
        if globalRowNumber == nil {
            globalRowNumber = 0
        }
        pickerView.selectRow(0, inComponent: 0, animated: true)
        texts = texts.sort { $0.lowercaseString < $1.lowercaseString }
        for i in 0...texts.count-1 {
            if texts[i] as! String == globalSelectedItem {
                globalRowNumber = i
            }
        }
        pickerView.selectRow(globalRowNumber!, inComponent: 0, animated: true)
        self.preSelectedLb = pickerView.viewForRow(globalRowNumber!, forComponent: 0) as! UILabel
    }
    
    //表示列
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //表示個数
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return texts.count
    }
    
    //表示内容
    //    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    //        return texts[row] as! String
    //    }
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView!) -> UIView {
        
        let pickerLabel = UILabel()
        let titleData = texts[row] as! String
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "GurmukhiMN", size: 25.0)!,NSForegroundColorAttributeName:UIColor.grayColor()])
        
        // fontサイズ、テキスト
        pickerLabel.attributedText = myTitle
        // 中央寄せ ※これを指定しないとセンターにならない
        pickerLabel.textAlignment = NSTextAlignment.Center
        pickerLabel.frame = CGRectMake(0, 0, 200, 30)
        
        // 既存ラベル、選択状態のラベルが存在している
        if let lb = pickerView.viewForRow(row, forComponent: component) as? UILabel,
            let selected = self.preSelectedLb {
            // 設定
            self.preSelectedLb = lb
            self.preSelectedLb.backgroundColor = UIColor.clearColor()
            self.preSelectedLb.textColor = UIColor.whiteColor()
        }
        
        return pickerLabel
    }
    
    //選択時
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        globalRowNumber = row
        //        pickerView.selectRow(globalRowNumber!, inComponent: 0, animated: true)
        globalSelectedItem = texts[row] as! String
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
