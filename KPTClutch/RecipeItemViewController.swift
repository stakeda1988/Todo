//
//  RecipeItemViewController.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 4/16/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

import UIKit
import CoreData

class RecipeItemViewController: UIViewController {
    
    @IBAction func cancel(sender: UIBarButtonItem) {
        navigationController!.popViewControllerAnimated(true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
