//
//  LaunchScreen.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 4/16/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

class LaunchScreen: UIViewController {
    var logoImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = UIColor.whiteColor()
        
        //imageView作成
        self.logoImageView = UIImageView(frame: CGRectMake(0, 0, 204, 77))
        //画面centerに
        self.logoImageView.center = self.view.center
        //logo設定
        self.logoImageView.image = UIImage(named: "recipe")
        //viewに追加
        self.view.addSubview(self.logoImageView)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        //少し縮小するアニメーション
        UIView.animateWithDuration(0.3,
                                   delay: 1.0,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: { () in
                                    self.logoImageView.transform = CGAffineTransformMakeScale(0.9, 0.9)
            }, completion: { (Bool) in
                
        })
        
        //拡大させて、消えるアニメーション
        UIView.animateWithDuration(0.2,
                                   delay: 1.3,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: { () in
                                    self.logoImageView.transform = CGAffineTransformMakeScale(1.2, 1.2)
                                    self.logoImageView.alpha = 0
            }, completion: { (Bool) in
                self.logoImageView.removeFromSuperview()
        })        
    }
}