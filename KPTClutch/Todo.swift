//
//  Todo.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 1/2/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

import Foundation
import CoreData

@objc(Todo)
class Todo: NSManagedObject {
    
    @NSManaged var item: String
    @NSManaged var remark: String
    
}