//
//  RecipeListViewController.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 4/16/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//

import UIKit
import CoreData

class RecipeListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    var todoEntities = [Todo]()
    var recipeEntities = [String]()
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        todoEntities = (Todo.MR_findAll() as? [Todo])!
        recipeEntities.append("Curry")
        print(todoEntities)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundView = nil
        tableView.estimatedRowHeight = 90
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
        
//        todoEntities = (Todo.MR_findAll() as? [Todo])!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todoEntities.count
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell:UITableViewCell = tableView.dequeueReusableCellWithIdentifier("TodoListItem")!
        
        cell.textLabel!.text = String(indexPath.row+1) + ". " + recipeEntities[indexPath.row]
        
        cell.backgroundColor = UIColor.clearColor()
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = cellSelectedBgView
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "GurmukhiMN", size: 15)
        cell.textLabel?.textColor = UIColor.whiteColor()
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "edit" {
            let todoController = segue.destinationViewController as! RecipeItemViewController
            let task = todoEntities[tableView.indexPathForSelectedRow!.row]
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}