//
//  Todo+CoreDataProperties.swift
//  KPTClutch
//
//  Created by SHOKI TAKEDA on 1/2/16.
//  Copyright © 2016 kptclutch.com. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Todo {

    @NSManaged var item: String?

}
